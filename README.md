CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works
 * Support requests
 * Maintainers


INTRODUCTION
------------

Social Auth Nextcloud is a Nextcloud authentication integration for
Drupal. It is based on the Social Auth and Social API projects

It adds to the site:

 * A new url: `/user/login/nextcloud:your_nextcloud_url`.

 * A settings form at `/admin/config/social-api/social-auth/nextcloud/nextcloud:your_nextcloud_url`.

 * A Nextcloud logo in the Social Auth Login block.


REQUIREMENTS
------------

This module requires the following modules:

 * [Social Auth](https://drupal.org/project/social_auth)
 * [Social API](https://drupal.org/project/social_api)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for more details.

As you need to define the nextcloud instances urls that could be set in your
drupal, you need to define these in settings.php adding:

```
$settings['social_auth_nextcloud_instances'] = [
  "nextcloud.example.org",
  "nuvo.example.org",
];
```

Note that this way of defining will change as soon as a config entity is done to
add instances via administration form.


CONFIGURATION
-------------

In Drupal:

 1. Log in as an admin.

 2. Navigate to Configuration » User authentication » 
   `Nextcloud your_nextcloud_url` and copy
   the Authorized redirect URI field value (the URL should end in
   `/user/login/nextcloud%3Ayour_nextcloud_url/callback`).

In Nextcloud:

 3. Log in to a Nextcloud account.

 4. Navigate to Settings » Administration Section >> Security » OAuth 2.0
    Clients section (/settings/admin/security)

 5. Click New OAuth App

 6. Set the Application name as desired.

 7. Paste the redirect URI value (from Step 2) in the Authorization callback
    URL field.

 8. Push the Add button.

 9. Copy the new secret key (Nextcloud will not show it again!) and Client
    Identifier and save them somewhere safe.

In Drupal:

 11. Return to Configuration » User authentication »
   `Nextcloud your_nextcloud_url`

 12. Enter the Nextcloud client ID in the Client ID field.

 13. Enter the Nextcloud secret key in the Client secret field.

 14. Click Save configuration.

 15. Navigate to Structure » Block Layout and place a Social Auth login block
     somewhere on the site (if not already placed).

That's it! Log in with a Nextcloud account to test the implementation.


HOW IT WORKS
------------

The user can click on the Nextcloud logo in the Social Auth Login block.
You can also add a button or link anywhere on the site that points
to `/user/login/nextcloud:your_nextcloud_url`, so theming and customizing the
button or link is very flexible.

After Nextcloud has returned the user to your site, the module compares the
user id or email address provided by Nextcloud. If the user has previously
registered using Nextcloud or your site already has an account with the same
email address, the user is logged in. If not, a new user account is created.
Also, a Nextcloud account can be associated with an authenticated user.


SUPPORT REQUESTS
----------------

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check the Recent Log entries at
  admin/reports/dblog

* Once you have done this, you can post a support request at module issue
  queue: [https://www.drupal.org/project/issues/social_auth_nextcloud](https://www.drupal.org/project/issues/social_auth_nextcloud)

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


MAINTAINERS
-----------

Current maintainers:

 * [Aleix Quintana Alsius (aleix)](https://www.drupal.org/u/aleix)

Development sponsored by:

 * [Communia](https://www.drupal.org/communia)
