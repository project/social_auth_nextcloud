<?php

namespace Drupal\social_auth_nextcloud\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Settings form for Social Auth Example.
 */
class NextcloudAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    $network = $this
      ->getRouteMatch()
      ->getParameter('network');
    return array_merge(["{$network->getBaseId()}.{$network->getDerivativeId()}.settings"], parent::getEditableConfigNames());
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'social_auth_nextcloud_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    $network = $this
      ->getRouteMatch()
      ->getParameter('network');
    return "{$network->getPluginId()}_settings";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NetworkInterface $network = NULL): array {
    // NetworkId reformatted to filter the colon from derivative plugin system.
    $config = $this->config("{$network->getBaseId()}.{$network->getDerivativeId()}.settings");
    $form = parent::buildForm($form, $form_state, $network);
    $form['network']['nextcloud_url'] = [
      '#weight' => -10,
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('nextcloud base url'),
      '#description' => $this->t('The base url of the nextcloud instance.'),
      '#default_value' => $config->get('nextcloud_url'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    /** @var \Drupal\social_auth\Plugin\Network\NetworkInterface $network */
    $network = $form_state->get('network');
    // NetworkId reformatted to filter the colon from derivative plugin system.
    $network_id = "{$network->getBaseId()}.{$network->getDerivativeId()}.settings";

    $this->configFactory->getEditable($network_id)
      ->set('client_id', $values['client_id'])
      ->set('client_secret', $values['client_secret'])
      ->set('scopes', $values['scopes'])
      ->set('endpoints', $values['endpoints'])
      ->set('nextcloud_url', $values['nextcloud_url'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
