<?php

namespace Drupal\social_auth_nextcloud\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates nextcloud_instances related local tasks.
 */
class DynamicLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Note about nextcloud instances config.
   *
   * @todo when adding Nextcloud auth instances entity config to be able to
   * list remove edit, allowed instances this logic must be adapted to obtain
   * $nextcloud_auth_providers from the entity config storage.
   * Take as example the:
   * \Drupal\media\Plugin\Derivative\DynamicLocalTasksSystemMenuBlock
   * The logic and entity storage must be the same in NextcloudInstance
   * derivative logic.
   */

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $nextcloud_auth_providers = Settings::get('social_auth_nextcloud_instances', []);
    foreach ($nextcloud_auth_providers as $item) {
      $this->derivatives["social_auth_nextcloud:" . $item . "settings.tab"] = [
        'title' => "Nextcloud " . $item,
        'route_name' => 'social_auth_nextcloud.settings_form',
        'route_parameters' => [
          'network' => 'nextcloud:' . $item,
        ],
        'base_route' => 'social_auth.integrations',
      ];
    }
    return $this->derivatives;
  }

}
