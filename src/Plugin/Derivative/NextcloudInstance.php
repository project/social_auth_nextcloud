<?php

namespace Drupal\social_auth_nextcloud\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides network plugin definitions for social auth nextcloud.
 *
 * @see \Drupal\system\Plugin\Block\SystemMenuBlock
 */
class NextcloudInstance extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /*
   * Note about instances config.
   *
   * @todo when adding Nextcloud auth instances entity config to be able to list
   * remove edit, allowed instances this logic must be adapted to obtain
   * $nextcloud_auth_providers from the entity config storage.
   * Take as example the \Drupal\system\Plugin\Derivative\SystemMenuBlock
   * The logic of getting entities must be the same in DynamicLocalTasks
   * derivative logic.
   */

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $nextcloud_auth_providers = Settings::get('social_auth_nextcloud_instances', []);
    foreach ($nextcloud_auth_providers as $item) {
      $this->derivatives[$item] = $base_plugin_definition;
      $this->derivatives[$item]['admin_label'] = $item;
      // This will be used to set the id.
      $this->derivatives[$item]['id'] = $base_plugin_definition['id'] . ":" . $item;
      // This will be used to set the alt url in block template.
      $this->derivatives[$item]['social_network'] = $this->t("Nextcloud at @base_url", ["@base_url" => $item]);
      // This will be used to build the block url redirect url from short_name.
      $this->derivatives[$item]['short_name'] = "nextcloud:" . $item;
      // NetworkId reformatted to filter colon from derivative plugin system.
      $network_settings_id = "{$base_plugin_definition['id']}.{$item}.settings";
      $this->derivatives[$item]['handlers']['settings']['config_id'] = $network_settings_id;
    }
    return $this->derivatives;
  }

}
