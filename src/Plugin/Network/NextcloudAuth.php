<?php

namespace Drupal\social_auth_nextcloud\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines a Network Plugin for Social Auth Nextcloud.
 *
 * @phpcs:disable
 * 
 * @package Drupal\social_auth_nextcloud\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_nextcloud",
 *   short_name = "nextcloud",
 *   social_network = "Nextcloud",
 *   img_path = "img/nextcloud_logo.svg",
 *   type = "social_auth",
 *   class_name = "\Bahuma\OAuth2\Client\Provider\Nextcloud",
 *   auth_manager = "\Drupal\social_auth_nextcloud\NextcloudAuthManager",
 *   routes = {
 *     "redirect": "social_auth.network.redirect",
 *     "callback": "social_auth.network.callback",
 *     "settings_form": "social_auth.network.settings_form",
 *   },
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_nextcloud\Settings\NextcloudAuthSettings",
 *       "config_id": "social_auth_nextcloud.settings"
 *     }
 *   },
 *   deriver = "Drupal\social_auth_nextcloud\Plugin\Derivative\NextcloudInstance"
 * )
 *
 * @phpcs:enable
 */
class NextcloudAuth extends NetworkBase implements NetworkInterface {

  /**
   * Gets additional settings for the network class.
   *
   * Implementors can declare and use this method to augment the settings array
   * passed to constructors for libraries that extend from a League abstract
   * provider.
   *
   * @return array
   *   Key-value pairs for extra settings to pass to the provider class
   *   constructor.
   *
   * @see \Drupal\social_auth\Plugin\Network\NetworkBase::initSdk()
   */
  protected function getExtraSdkSettings(): array {
    return ['nextcloudUrl' => $this->settings->getNextcloudUrl()];
  }

}
