<?php

namespace Drupal\social_auth_nextcloud\Settings;

use Drupal\social_auth\Settings\SettingsBase;

/**
 * Defines settings for Nextcloud Social Auth provider.
 *
 * @package Drupal\social_auth\Settings
 */
class NextcloudAuthSettings extends SettingsBase implements NextcloudAuthSettingsInterface {

  /**
   * Nextcloud Url.
   *
   * @var string|null
   */
  protected ?string $nextcloudUrl = NULL;

  /**
   * {@inheritdoc}
   */
  public function getNextcloudUrl(): ?string {
    if (!$this->nextcloudUrl) {
      $this->nextcloudUrl = $this->config->get('nextcloud_url');
    }
    return $this->nextcloudUrl;
  }

}
