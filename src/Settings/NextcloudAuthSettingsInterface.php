<?php

namespace Drupal\social_auth_nextcloud\Settings;

use Drupal\social_auth\Settings\SettingsInterface;

/**
 * Defines an interface for Social Auth Nextcloud settings.
 */
interface NextcloudAuthSettingsInterface extends SettingsInterface {

  /**
   * Gets the Nextcloud base url.
   *
   * @return string|null
   *   The base url.
   */
  public function getNextcloudUrl(): ?string;

}
