<?php

namespace Drupal\Tests\social_auth_nextcloud\Functional;

/**
 * Test that path to authentication route exists in Social Auth Login block.
 *
 * @group social_auth_nextcloud
 */
class SocialAuthNextcloudLoginBlockTest extends SocialAuthNextcloudTestBase {

  /**
   * Test that the path is included in the login block.
   */
  public function testLinkExistsInBlock() {
    $this->settingNextcloudInstances();
    $this->checkLinkToProviderExists();
  }

}
