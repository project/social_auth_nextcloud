<?php

namespace Drupal\Tests\social_auth_nextcloud\Functional;

/**
 * Test Social Auth Nextcloud settings form.
 *
 * @group social_auth_nextcloud
 */
class SocialAuthNextcloudSettingsFormTest extends SocialAuthNextcloudTestBase {

  /**
   * Test if implementer is shown in the integration list.
   */
  public function testIsAvailableInIntegrationList() {

    $this->settingNextcloudInstances();
    $this->fields = ['client_id', 'client_secret', 'nextcloud_url'];

    $this->checkIsAvailableInIntegrationList();
  }

  /**
   * Test if permissions are set correctly for settings page.
   */
  public function testPermissionForSettingsPage() {
    $this->settingNextcloudInstances();
    $this
      ->rebuildContainer();

    $this->checkPermissionForSettingsPage();
  }

  /**
   * Test settings form submission.
   */
  public function testSettingsFormSubmission() {
    $this->settingNextcloudInstances();

    $this->edit = [
      'client_id' => $this->randomString(10),
      'client_secret' => $this->randomString(10),
      'nextcloud_url' => 'https://example.com',
    ];

    $this->checkDerivativeSettingsFormSubmission();
  }

}
