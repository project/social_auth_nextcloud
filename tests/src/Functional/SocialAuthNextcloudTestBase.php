<?php

namespace Drupal\Tests\social_auth_nextcloud\Functional;

use Drupal\Core\Url;
use Drupal\Tests\social_auth\Functional\SocialAuthTestBase;

/**
 * Defines a base class for testing Social Auth Nextcloud.
 *
 * @group social_auth_nextcloud
 */
abstract class SocialAuthNextcloudTestBase extends SocialAuthTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['social_auth_nextcloud'];

  /**
   * Social Auth another provider.
   *
   * @var string
   */
  protected $providerOther;

  /**
   * Social Auth Next cloud instances.
   *
   * @var array
   */
  protected $socialAuthNextcloudInstances;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->module = 'social_auth_nextcloud';
    $this->provider = 'nextcloud:example.com';
    $this->providerOther = 'nextcloud:otherexample.com';
    $this->moduleType = 'social-auth';
    $this->socialAuthNextcloudInstances = ['example.com', 'otherexample.com'];
    parent::setUp();
  }

  /**
   * Set Nextcloud Instances in settings.
   */
  protected function settingNextcloudInstances() {
    $settings['settings']['social_auth_nextcloud_instances'] = (object) [
      'value' => $this->socialAuthNextcloudInstances,
      'required' => TRUE,
    ];
    $this->writeSettings($settings);
    $this
      ->rebuildContainer();
  }

  /**
   * Tests module settings form submission.
   */
  protected function checkDerivativeSettingsFormSubmission() {
    $this->drupalLogin($this->adminUser);
    $path = 'admin/config/social-api/' . $this->moduleType . '/nextcloud/' . $this->provider;
    $this->drupalGet($path);
    $this->submitForm($this->edit, $this->t('Save configuration'));
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
  }

  /**
   * Test if link to provider exists in login block.
   */
  protected function checkLinkToProviderExists() {
    // Test for a non-authenticated user.
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->pageTextContains($this->socialAuthLoginBlock->label());

    if ($this->provider) {
      $this->checkPathInBlock($this->authRootPath . urlencode($this->provider));
    }
    if ($this->providerOther) {
      $this->checkPathInBlock($this->authRootPath . urlencode($this->providerOther));
    }

    // Test for an authenticated user.
    $this->drupalLogin($this->noPermsUser);
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->pageTextContains($this->socialAuthLoginBlock->label());

    if ($this->provider) {
      $this->checkPathInBlock($this->authRootPath . urlencode($this->provider));
    }
    if ($this->providerOther) {
      $this->checkPathInBlock($this->authRootPath . urlencode($this->providerOther));
    }

  }

}
